#!/bin/sh

getname=$(kubectl get namespace java | grep java | awk '{print $1}')

if [ $getname == 'java' ]
then
kubectl delete namespace java
kubectl create -f my-namespace.yaml
else
kubectl create -f my-namespace.yaml
fi

getservice=$(kubectl get serviceaccount java-jayashree --namespace java |grep java-jayashree | awk '{print $1}')

if [ $getservice == 'java-jayashree' ]
then
kubectl delete serviceaccount java-jayashree -n java
kubectl create -f service-account.yaml
else
kubectl create -f service-account.yaml
fi

getuser=$(kubectl get role java-jayashree-full-access -n java | grep java-jayashree-full-access | awk '{print $1}')

if [ $getuser == 'java-jayashree-full-access' ]
then
kubectl delete  role java-jayashree-full-access -n java
kubectl create -f role.yaml
else
kubectl create -f role.yaml
fi

getrolebind=$(kubectl get role java-jayashree-view -n java | grep java-jayashree-view | awk '{print $1')

if [ $getrolebind == 'java-jayashree-view' ]
then
kubectl delete  rolebinding java-jayashree-view -n java
kubectl create -f role-binding.yaml
else
kubectl create -f role-binding.yaml
fi
